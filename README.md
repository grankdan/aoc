# Advent Of Code
My humble solutions to [Advent of Code](https://adventofcode.com/)

First time coding in Python, so this is my way of learning it.

Thanks to [Ondra Skalicka](https://gitlab.com/OndrejSkalicka) for pushing me towards better (programmer) me :D