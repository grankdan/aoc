import numpy
from dataclasses import dataclass


@dataclass
class Monkey:
    items: list[int]
    operator: str
    argument_type: str
    argument_value: int
    modulo: int
    test_success_path: int
    test_failure_path: int
    inspections = 0


def first(input_file_name: str) -> int:
    """
    >>> first('d11_monkey_in_the_middle_test.input')
    10605
    >>> first('d11_monkey_in_the_middle.input')
    110888
    """
    return monkey_business(input_file_name, rounds=20, calmness=3)


def second(input_file_name: str) -> int:
    """
    >>> second('d11_monkey_in_the_middle_test.input')
    2713310158
    >>> second('d11_monkey_in_the_middle.input')
    25590400731
    """
    return monkey_business(input_file_name, rounds=10000, calmness=1)


def monkey_business(input_file_name: str, rounds: int, calmness: int) -> int:
    with open(input_file_name, 'r') as data_file:
        data_input = data_file.readlines()
        monkeys = _parse_data(data_input)
        stress_modulo = numpy.prod([monkey.modulo for monkey in monkeys])

        for _ in range(rounds):
            for monkey in monkeys:
                monkey.inspections += len(monkey.items)
                for item in monkey.items:
                    monkey_inspect(item, monkey, monkeys, stress_modulo, calmness)
                monkey.items = []

        inspections = [monkey.inspections for monkey in monkeys]
        inspections.sort()
        return inspections[-1] * inspections[-2]


def monkey_inspect(item: int, monkey: Monkey, monkeys: list[Monkey], stress_modulo: int, calmness: int):
    parameter = monkey.argument_value if monkey.argument_type == "integer" else item

    new_worry_level = item * parameter if monkey.operator == "*" else item + parameter

    new_worry_level %= stress_modulo

    if calmness != 1:
        new_worry_level = new_worry_level // calmness

    divisible = new_worry_level % monkey.modulo == 0
    next_monkey = monkey.test_success_path if divisible else monkey.test_failure_path
    monkeys[next_monkey].items.append(new_worry_level)


def _parse_data(data_input: list[str]) -> list[Monkey]:
    monkeys: list[Monkey] = []

    data_input = list(filter(lambda line: line != "\n", data_input))

    for starting_items, operation, test_def, true_path, false_path \
            in zip(data_input[1::6], data_input[2::6], data_input[3::6], data_input[4::6], data_input[5::6]):
        items = [int(x.strip()) for x in starting_items[18:].split(',')]
        operator = operation[23]
        argument_type = "old_value" if operation[25:] == "old\n" else "integer"
        argument_value = -1 if argument_type == "old_value" else int(operation[25:])
        division_test_value = int(test_def[21:])
        test_success_path = int(true_path[29:])
        test_failure_path = int(false_path[30:])

        monkeys.append(
            Monkey(items, operator, argument_type, argument_value, division_test_value, test_success_path,
                   test_failure_path))

    return monkeys


if __name__ == "__main__":
    print(first('d11_monkey_in_the_middle.input'))
    print(second('d11_monkey_in_the_middle.input'))
