from anytree import Node


def first(input_file_name: str) -> int:
    """
    >>> first('d7_file_cleanup_test.input')
    95437
    >>> first('d7_file_cleanup.input')
    1315285
    """

    with open(input_file_name, 'r') as data_file:
        root = read_tree(data_file.readlines())
        return check_sizes(root)


def check_sizes(node: Node) -> int:
    amount_of_large_dirs: int = node.size if node.size <= 100000 else 0

    for child in [child for child in node.children if child.dir]:
        amount_of_large_dirs += check_sizes(child)
    return amount_of_large_dirs


def read_tree(terminal_output: list[str]) -> Node:
    root = Node("/", size=0, dir=True)

    current_dir = root

    for line in terminal_output:
        line_parts = line.split()

        if line_parts[0] == "$":

            if line_parts[1] == "cd":
                destination = line_parts[2]
                if destination == "/":
                    current_dir = root
                elif destination == "..":
                    current_dir = current_dir.parent
                else:
                    next_dir = next(x for x in current_dir.children if x.name == destination)
                    current_dir = next_dir
            elif line_parts[1] == "ls":
                continue
            else:
                raise Exception("Unsupported command %s", line_parts[1])

        else:
            if line_parts[0] == "dir":
                Node(line_parts[1], current_dir, size=0, dir=True)
            else:
                Node(line_parts[1], current_dir, size=int(line_parts[0]), dir=False)

    update_sizes(root)
    return root


def update_sizes(node: Node) -> int:
    for child in node.children:
        child_size = update_sizes(child)
        node.size += child_size
    return node.size


def second(input_file_name: str) -> int:
    """
    >>> second('d7_file_cleanup_test.input')
    24933642
    >>> second('d7_file_cleanup.input')
    9847279
    """

    with open(input_file_name, 'r') as data_file:
        root = read_tree(data_file.readlines())
        space_needed = 30000000 - (70000000 - root.size)
        dir_to_delete = find_dir_to_delete(root, space_needed)
        return dir_to_delete.size


def find_dir_to_delete(node: Node, space_needed: int) -> Node:

    result = node

    for child in [child for child in node.children if child.dir]:
        candidate = find_dir_to_delete(child, space_needed)
        if space_needed <= candidate.size < result.size:
            result = candidate
    return result


if __name__ == "__main__":
    print(first('d7_file_cleanup.input'))
    print(second('d7_file_cleanup.input'))
