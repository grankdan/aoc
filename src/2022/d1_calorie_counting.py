def first(input_file_name: str) -> int:
    """
    >>> first('d1_calorie_counting_test.input')
    24000
    """

    with open(input_file_name, 'r') as data_file:
        data = data_file.readlines()

        elves = []

        calories_carried_by_elf = 0
        for line in data:
            if line == "\n":
                elves.append(calories_carried_by_elf)
                calories_carried_by_elf = 0
            else:
                calories_carried_by_elf += int(line)

        if calories_carried_by_elf != 0:
            elves.append(calories_carried_by_elf)

    elves.sort()
    return elves[-1]


def second(input_file_name: str) -> int:
    """
    >>> second('d1_calorie_counting_test.input')
    45000
    """

    with open(input_file_name, 'r') as data_file:
        data = data_file.readlines()

        elves = []

        calories_carried_by_elf = 0
        for line in data:
            if line == "\n":
                elves.append(calories_carried_by_elf)
                calories_carried_by_elf = 0
            else:
                calories_carried_by_elf += int(line)

        if calories_carried_by_elf != 0:
            elves.append(calories_carried_by_elf)

    elves.sort()
    return sum(elves[-3:])


if __name__ == "__main__":
    print(first('d1_calorie_counting.input'))
    print(second('d1_calorie_counting.input'))
