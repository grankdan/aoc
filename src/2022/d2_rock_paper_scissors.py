def first(input_file_name: str) -> int:
    """
    >>> first('d2_rock_paper_scissors_test.input')
    15
    """

    with open(input_file_name, 'r') as data_file:
        data = data_file.readlines()

        score = 0
        for line in data:
            game_round = line.split()

            his_hand = ord(game_round[0]) - ord("A")
            my_hand = ord(game_round[-1]) - ord("X")

            score += my_hand + 1

            if my_hand == his_hand:
                score += 3

            elif (my_hand - 1) % 3 == his_hand:
                score += 6

        return score


def second(input_file_name: str) -> int:
    """
    >>> second('d2_rock_paper_scissors_test.input')
    12
    """

    with open(input_file_name, 'r') as data_file:
        data = data_file.readlines()

        score = 0
        for line in data:
            game_round = line.split()

            his_hand = ord(game_round[0]) - ord("A")
            my_action = ord(game_round[-1]) - ord("X")

            score += my_action * 3

            score += (his_hand + my_action - 1) % 3 + 1
        return score


if __name__ == "__main__":
    print(first('d2_rock_paper_scissors.input'))
    print(second('d2_rock_paper_scissors.input'))
