from dataclasses import dataclass
import math


@dataclass
class Tree:
    height: int
    seen: bool
    scenic_score: list[int]


def first(input_file_name: str) -> int:
    """
    >>> first('d8_treetop_tree_house_test.input')
    21
    >>> first('d8_treetop_tree_house.input')
    1688
    """

    grid: list[list[Tree]] = read_forest(input_file_name)
    observe(grid)

    observed_trees = 0

    for line in grid:
        for tree in line:
            if tree.seen:
                observed_trees += 1

    return observed_trees


def observe(grid: list[list[Tree]]):
    for _ in range(4):
        look_from_left(grid)
        grid = rotate(grid)


def look_from_left(grid: list[list[Tree]]):
    for line in grid:
        last_tallest_tree = -1
        for tree in line:
            if last_tallest_tree < tree.height:
                tree.seen = True
                last_tallest_tree = tree.height


def rotate(array_2d: list[list[Tree]]):
    list_of_tuples = zip(*array_2d[::-1])
    return [list(elem) for elem in list_of_tuples]


def read_forest(input_file_name: str) -> list[list[Tree]]:
    with open(input_file_name, 'r') as data_file:
        lines = data_file.readlines()
        grid: list[list[Tree]] = []

        for line_index in range(len(lines)):
            grid.append([])
            for tree in lines[line_index].strip():
                grid[line_index].append(Tree(int(tree), False, [0, 0, 0, 0]))

        return grid


def second(input_file_name: str) -> int:
    """
    >>> second('d8_treetop_tree_house_test.input')
    8
    >>> second('d8_treetop_tree_house.input')
    410400
    """

    grid = read_forest(input_file_name)
    test(grid, 0)
    test(grid, 1)
    test(grid, 2)
    test(grid, 3)

    max_scenic_score = 0

    for line in grid:
        for tree in line:
            tree_scenic_score = math.prod(tree.scenic_score)
            if max_scenic_score < tree_scenic_score:
                max_scenic_score = tree_scenic_score

    return max_scenic_score


def test(grid: list[list[Tree]], rotation_cycle: int):
    for _ in range(rotation_cycle):
        grid = rotate(grid)

    for line in grid:
        for tree_index in range(len(line)):
            tree = line[tree_index]
            prev_trees = line[:tree_index]
            prev_trees.reverse()
            for prev_tree in prev_trees:
                tree.scenic_score[rotation_cycle] += 1
                if prev_tree.height >= tree.height:
                    break


if __name__ == "__main__":
    print(first('d8_treetop_tree_house.input'))
    print(second('d8_treetop_tree_house.input'))
