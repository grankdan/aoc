def first(input_file_name: str) -> int:
    """
    >>> first('d4_camp_cleanup_test.input')
    2
    """

    with open(input_file_name, 'r') as data_file:
        data = data_file.readlines()

        fully_contained_assignments_count = 0
        for line in data:
            both_assignments = line.split(",")
            first_range = set(parse_range(both_assignments, 0))
            second_range = set(parse_range(both_assignments, 1))

            if first_range.issubset(second_range) or second_range.issubset(first_range):
                fully_contained_assignments_count += 1

        return fully_contained_assignments_count


def second(input_file_name: str) -> int:
    """
    >>> second('d4_camp_cleanup_test.input')
    4
    """

    with open(input_file_name, 'r') as data_file:
        data = data_file.readlines()

        intersect_count = 0
        for line in data:
            both_assignments = line.split(",")
            first_range = set(parse_range(both_assignments, 0))
            second_range = set(parse_range(both_assignments, 1))

            if first_range.intersection(second_range):
                intersect_count += 1

        return intersect_count


def parse_range(both_assignments: list[str], index: int) -> range:
    range_int_list = list(map(int, both_assignments[index].split("-")))

    return range(range_int_list[0], range_int_list[1] + 1)


if __name__ == "__main__":
    print(first('d4_camp_cleanup.input'))
    print(second('d4_camp_cleanup.input'))
