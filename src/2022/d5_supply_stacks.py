def first(input_file_name: str) -> str:
    """
    >>> first('d5_supply_stacks_test.input')
    'CMZ'
    """

    with open(input_file_name, 'r') as data_file:
        data_input = data_file.readlines()

        ship = parse_init_state(data_input)

        result = ""

        action_start_index = data_input.index("\n") + 1

        for line in data_input[action_start_index:]:
            action = line.split(" ")

            amount, source, destination = int(action[1]), int(action[3]) - 1, int(action[5]) - 1

            for _ in range(amount):
                ship[destination] += ship[source][-1]
                ship[source] = ship[source][:-1]

        for stack in ship:
            result += stack.pop()

        return result


def parse_init_state(data_input: list[str]) -> list[list[str]]:

    input_delimiter_index = data_input.index("\n")
    container_numbers_row_index = input_delimiter_index - 1

    number_of_stacks = int(data_input[container_numbers_row_index].strip().split(" ")[-1])

    ship: list[list[str]] = []

    for _ in range(number_of_stacks):
        ship.append([])

    for line in reversed(data_input[:container_numbers_row_index]):
        parse_index = 1

        while parse_index < len(line):
            if line[parse_index] != " ":
                ship[(parse_index - 1) // 4].append(line[parse_index])
            parse_index += 4

    return ship


def second(input_file_name: str) -> str:
    """
    >>> second('d5_supply_stacks_test.input')
    'MCD'
    """

    with open(input_file_name, 'r') as data_file:
        data_input = data_file.readlines()

        ship = parse_init_state(data_input)

        result = ""

        action_start_index = data_input.index("\n") + 1

        for line in data_input[action_start_index:]:
            action = line.split(" ")

            amount, source, destination = int(action[1]), int(action[3]) - 1, int(action[5]) - 1

            ship[destination] += ship[source][-amount:]

            ship[source] = ship[source][:-amount]

        for stack in ship:
            result += stack.pop()

        return result


if __name__ == "__main__":
    print(first('d5_supply_stacks.input'))
    print(second('d5_supply_stacks.input'))
