probe_cycles = {20, 60, 100, 140, 180, 220}


def first(input_file_name: str) -> int:
    """
    >>> first('d10_cathode_ray_tube_test.input')
    13140
    >>> first('d10_cathode_ray_tube.input')
    17180
    """
    with open(input_file_name, 'r') as data_file:
        data_input = data_file.readlines()
        return _video_system(data_input, output_image=False)


def second(input_file_name: str):
    with open(input_file_name, 'r') as data_file:
        data_input = data_file.readlines()
        _video_system(data_input, output_image=True)


def _video_system(data_input: list[str], output_image: bool) -> int:
    crt = [""] * 6

    sprite_position = 1
    crt_position = 0
    cycle = 0
    probe_values = []

    for line in data_input:
        crt_position, cycle = next_cycle(crt, crt_position, cycle, probe_values, sprite_position)

        if line[:4] == "addx":
            crt_position, cycle = next_cycle(crt, crt_position, cycle, probe_values, sprite_position)
            sprite_position += int(line[5:-1])

    if output_image:
        for row in crt:
            print(row)

    return sum(probe_values)


def next_cycle(crt, crt_position, cycle, probe_values, sprite_position):
    cycle += 1
    if cycle in probe_cycles:
        probe_values.append(cycle * sprite_position)
    crt[crt_position // 40] += "." if abs(sprite_position - crt_position % 40) > 1 else "#"
    crt_position += 1
    return crt_position, cycle


if __name__ == "__main__":
    print(first('d10_cathode_ray_tube.input'))
    second('d10_cathode_ray_tube.input')
