import json
from functools import cmp_to_key
from typing import Any


def first(input_file_name: str) -> int:
    """
    >>> first('d13_distress_signal_test.input')
    13
    >>> first('d13_distress_signal.input')
    5503
    """
    with open(input_file_name, 'r') as data_file:
        data_input = data_file.readlines()

        indexes_sum = 0

        for index in range(0, len(data_input), 3):

            left = json.loads(data_input[index])
            right = json.loads(data_input[index + 1])

            if _compare(left, right) <= 0:
                indexes_sum += (index + 3) // 3

        return indexes_sum


def second(input_file_name: str):
    """
    >>> second('d13_distress_signal_test.input')
    140
    >>> second('d13_distress_signal.input')
    430
    """
    with open(input_file_name, 'r') as data_file:
        data_input = data_file.readlines()

        first_divider = [[2]]
        second_divider = [[6]]

        signals = [first_divider, second_divider]

        for line in data_input:
            if line != "\n":
                signals.append(json.loads(line))

        ordered_signals = sorted(signals, key=cmp_to_key(_compare))

        first_divider_index = ordered_signals.index(first_divider)
        second_divider_index = ordered_signals.index(second_divider)
        return first_divider_index * second_divider_index


def _compare(left: Any, right: Any) -> int:
    if isinstance(left, list) and isinstance(right, list):
        for index in range(min(len(left), len(right))):
            result = _compare(left[index], right[index])
            if result != 0:
                return result
        return len(left) - len(right)

    if isinstance(left, int):
        if isinstance(right, int):
            return left - right
        else:
            return _compare([left], right)

    return _compare(left, [right])


if __name__ == "__main__":
    print(first('d13_distress_signal.input'))
    print(second('d13_distress_signal.input'))
