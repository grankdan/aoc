import collections


def first(input_file_name: str) -> int:
    """
    >>> first('d12_hill_climbing_algorithm_test.input')
    31
    >>> first('d12_hill_climbing_algorithm.input')
    437
    """
    return walk(input_file_name)


def second(input_file_name: str):
    """
    >>> second('d12_hill_climbing_algorithm_test.input')
    29
    >>> second('d12_hill_climbing_algorithm.input')
    430
    """
    return walk(input_file_name, reverse=True)


def walk(input_file_name: str, reverse: bool = False) -> int:
    with open(input_file_name, 'r') as data_file:
        data_input = data_file.readlines()
        grid: list[list[str]] = []

        start: (int, int)

        for row_index, row in enumerate(data_input):
            line = []
            grid.append(line)
            for char_index, char in enumerate(row.removesuffix("\n")):
                line.append(char)
                if char == ('E' if reverse else 'S'):
                    start = (char_index, row_index)

        return len(bfs(grid, start, reverse)) - 1


def bfs(grid: list[list[str]], start: (int, int), reverse: bool) -> list[(int, int)]:
    queue = collections.deque([[start]])
    seen = {start}

    height = len(grid)
    width = len(grid[0])

    while queue:
        path = queue.popleft()
        x, y = path[-1]
        if grid[y][x] == ('a' if reverse else 'E'):
            return path
        for x2, y2 in ((x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)):
            if 0 <= x2 < width and 0 <= y2 < height:

                if (x2, y2) not in seen:

                    if _can_step(grid[y][x], grid[y2][x2], reverse=reverse):

                        queue.append(path + [(x2, y2)])
                        seen.add((x2, y2))


def _can_step(point_a: str, point_b: str, reverse: bool = False) -> bool:
    if reverse:
        return _height(point_a) - _height(point_b) < 2
    else:
        return _height(point_b) - _height(point_a) < 2


def _height(char: str) -> int:
    if char == 'S':
        return ord('a')
    elif char == 'E':
        return ord('z')
    else:
        return ord(char)


if __name__ == "__main__":
    print(first('d12_hill_climbing_algorithm.input'))
    print(second('d12_hill_climbing_algorithm.input'))
