from collections import Counter


def first(input_file_name: str) -> int:
    """
    >>> first('d6_tuning_trouble_test.input')
    7
    """

    with open(input_file_name, 'r') as data_file:
        line = data_file.readline()
        return _radio(line, 4)


def second(input_file_name: str) -> int:
    """
    >>> second('d6_tuning_trouble_test.input')
    19
    """

    with open(input_file_name, 'r') as data_file:
        line = data_file.readline()
        return _radio(line, 14)


def _radio(datastream: str, marker_length: int) -> int:
    for index in range(len(datastream) - marker_length):
        if len(Counter(datastream[index:marker_length + index])) == marker_length:
            return marker_length + index

    raise Exception("Did not find the marker")


if __name__ == "__main__":
    print(first('d6_tuning_trouble.input'))
    print(second('d6_tuning_trouble.input'))
