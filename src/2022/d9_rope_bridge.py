import numpy as np

move_map = {"R": (1, 0),
            "L": (-1, 0),
            "U": (0, 1),
            "D": (0, -1)}


def first(input_file_name: str) -> int:
    """
    >>> first('d9_rope_bridge_test.input')
    13
    >>> first('d9_rope_bridge.input')
    5858
    """
    with open(input_file_name, 'r') as data_file:
        data_input = data_file.readlines()
        return _engine(data_input, 2)


def second(input_file_name: str) -> int:
    """
    >>> second('d9_rope_bridge_test_larger.input')
    36
    >>> second('d9_rope_bridge.input')
    2602
    """
    with open(input_file_name, 'r') as data_file:
        data_input = data_file.readlines()
        return _engine(data_input, 10)


def _engine(data_input: list[str], length: int) -> int:
    rope = [(0, 0)] * length
    visited_positions = {(0, 0)}

    for line in data_input:
        move_direction, move_distance = line.split(' ')
        for _ in range(int(move_distance)):
            move_delta = move_map.get(move_direction)
            rope[0] = (rope[0][0] + move_delta[0], rope[0][1] + move_delta[1])

            for i in range(length - 1):
                delta_x = rope[i][0] - rope[i + 1][0]
                delta_y = rope[i][1] - rope[i + 1][1]
                if abs(delta_x) > 1 or abs(delta_y) > 1:
                    rope[i + 1] = (rope[i + 1][0] + np.sign(delta_x), rope[i + 1][1] + np.sign(delta_y))

            visited_positions.add(rope[-1])

    return len(visited_positions)


if __name__ == "__main__":
    print(first('d9_rope_bridge.input'))
    print(second('d9_rope_bridge.input'))
