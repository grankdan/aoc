def first(input_file_name: str) -> int:
    """
    >>> first('d3_backpack_test.input')
    157
    """

    with open(input_file_name, 'r') as data_file:
        data = data_file.readlines()

        item_priority_sum = 0
        for line in data:
            item = list(set(line[:len(line) // 2]) & set(line[len(line) // 2:]))[0]

            item_priority_sum += (ord(item) - 38) % 58

        return item_priority_sum


def second(input_file_name: str) -> int:
    """
    >>> second('d3_backpack_test.input')
    70
    """

    with open(input_file_name, 'r') as data_file:
        data = data_file.readlines()

        badge_priority_sum = 0
        for i in range(0, len(data), 3):
            item = list(set(data[i][:-1]) & set(data[i + 1][:-1]) & set(data[i + 2][:-1]))[0]

            badge_priority_sum += (ord(item) - 38) % 58

        return badge_priority_sum


if __name__ == "__main__":
    print(first('d3_backpack.input'))
    print(second('d3_backpack.input'))
